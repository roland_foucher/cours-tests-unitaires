package co.simplon.alt3;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Human {
  private String name;
  private LocalDate birthDate;
  private String adress;
  private List<Dog> dogs;
  private static final int MAX_DOG = 5;

  public Human() {
  }

  public Human(String name, LocalDate birthDay, String adress) throws InvalidBirthdateException {
    setName(name);
    setBirthdate(birthDay);
    this.adress = adress;
    this.dogs = new ArrayList<>();
  }

  public String getName() {
    return name;
  }

  public LocalDate getBirthDay() {
    return birthDate;
  }

  public String getAdress() {
    return adress;
  }

  public void setBirthdate(LocalDate birthdate) throws InvalidBirthdateException {

    if (birthdate.isBefore(LocalDate.now().plusDays(1))) {
      this.birthDate = birthdate;
    } else {
      throw new InvalidBirthdateException();
    }
  }

  public void addDog(Dog dog) throws TooMuchDogForOwner {
    if (getNumberOfDogs() >= getMaxDog()) {
      throw new TooMuchDogForOwner();
    }
    dogs.add(dog);
    dog.setOwner(this);
  }

  public int getNumberOfDogs() {
    return dogs.size();
  }

  public Dog getOneDog(int index) {
    return dogs.get(index);
  }

  public void addMultipleDogs(Dog... dogs) throws TooMuchDogForOwner {
    for (Dog dog : dogs) {
      addDog(dog);
    }
  }

  public void setName(String name) {
    if (name == null || name.isBlank()) {
      throw new IllegalArgumentException("name is null or empty !");
    }
    this.name = wordSynthax(name, " ", "-");
  }

  public String wordSynthax(String name, String... regexArray) {
    String newString = name.toLowerCase();

    for (String regex : regexArray) {
      newString = Arrays
          .stream(newString.split("[" + regex + "]"))
          .map(x -> x.substring(0, 1).toUpperCase() + x.substring(1))
          .collect(Collectors.joining(regex));
    }

    return newString;
  }

  public static int getMaxDog() {
    return MAX_DOG;
  }
}
