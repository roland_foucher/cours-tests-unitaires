package co.simplon.alt3;

import java.time.LocalDate;

public class Dog {
  private String name;
  private LocalDate birthdate;
  private Human owner;
  private double price;

  public Dog(String name, LocalDate birthdate, double price) throws InvalidBirthdateException {
    this.name = name;
    setBirthdate(birthdate);
    this.price = price;
  }

  public String getName() {
    return name;
  }

  public LocalDate getBirthdate() {
    return birthdate;
  }

  public Human getProprio() {
    return owner;
  }

  public double getPrice() {
    return price;
  }

  public void setBirthdate(LocalDate birthdate) throws InvalidBirthdateException {
    if (birthdate.isBefore(LocalDate.now().plusDays(1))) {
      this.birthdate = birthdate;
    } else {
      throw new InvalidBirthdateException();
    }
  }

  public Human getOwner() {
    return owner;
  }

  public void setOwner(Human owner) {
    this.owner = owner;
  }
}
