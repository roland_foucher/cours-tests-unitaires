package co.simplon.alt3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

public class DogTest {
  @Test
  void testSetBirthdateToday() throws InvalidBirthdateException {
    Dog dog = new Dog("name", LocalDate.now(), 12);
    assertNotNull(dog);
    assertEquals("name", dog.getName());
  }

  @Test
  void testSetBirthdateBeforeToday() throws InvalidBirthdateException {
    Dog dog = new Dog("name", LocalDate.of(2012, 12, 24), 12);
    assertNotNull(dog);
    assertEquals("name", dog.getName());
  }

  @Test
  void testSetBirthdateAfterToday() throws InvalidBirthdateException {

    // junit
    assertThrows(InvalidBirthdateException.class,
        () -> new Dog("name", LocalDate.now().plusDays(1), 12));

    // assertJ
    assertThatThrownBy(() -> new Dog("name", LocalDate.now().plusDays(1), 12))
        .isInstanceOf(InvalidBirthdateException.class)
        .hasMessageContaining("this date is in the futur 🤨");
  }
}
