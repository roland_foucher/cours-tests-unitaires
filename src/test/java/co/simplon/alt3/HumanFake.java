package co.simplon.alt3;

import java.time.LocalDate;

import co.simplon.alt3.Human;
import co.simplon.alt3.InvalidBirthdateException;

public class HumanFake extends Human {

  @Override
  public int getNumberOfDogs() {
    return Human.getMaxDog() + 1;
  }
}
