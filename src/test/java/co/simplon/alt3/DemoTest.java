package co.simplon.alt3;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

public class DemoTest {
  @Test
  void testCalc() {
    assertThat(Demo.calc(1, 2)).isEqualTo(3);
  }

  @Test
  void testDiv() {
    assertThat(Demo.division(1, 2)).isEqualTo(0);
    assertThatThrownBy(() -> Demo.division(1, 0)).isInstanceOf(ArithmeticException.class)
        .hasMessageContaining("by zero");
  }
}
