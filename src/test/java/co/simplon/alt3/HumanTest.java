package co.simplon.alt3;

import java.time.LocalDate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class HumanTest {
  private Human human;
  private Dog dog1;
  private Dog dog2;
  private Dog dog3;
  private Dog dog4;
  private Dog dog5;
  private Dog dog6;
  private String regexWord = "^[A-Z][a-z]*([ -][A-Z][a-z]*)*$";

  @BeforeEach
  void init() throws InvalidBirthdateException {
    human = new Human("name", LocalDate.of(1988, 12, 24), "23 rue du champs 69003 LYON");
    dog1 = new Dog("name1", LocalDate.now(), 12);
    dog2 = new Dog("name2", LocalDate.now(), 12);
    dog3 = new Dog("name3", LocalDate.now(), 12);
    dog4 = new Dog("name4", LocalDate.now(), 12);
    dog5 = new Dog("name5", LocalDate.now(), 12);
    dog6 = new Dog("name6", LocalDate.now(), 12);
  }

  @Test
  void testAddDogs() throws InvalidBirthdateException, TooMuchDogForOwner {
    human.addDog(dog1);
    human.addDog(dog2);
    human.addMultipleDogs(dog3, dog4);
    assertThat(human.getNumberOfDogs()).isNotEqualTo(0);
    assertThat(human.getOneDog(1).getName()).isEqualTo("name2");
  }

  @Test
  void addMoreThan5Dogs() throws TooMuchDogForOwner {
    assertThatThrownBy(() -> human.addMultipleDogs(new DummyDog(), new DummyDog(), new DummyDog(), new DummyDog(),
        new DummyDog(), new DummyDog()))
        .isInstanceOf(TooMuchDogForOwner.class)
        .hasMessage("this human have more than 5 Dogs");
  }

  @Test
  void nameNotEmpty() {
    assertThatThrownBy(() -> human.setName(""))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("name is null or empty");

    assertThatThrownBy(() -> new Human("", LocalDate.now(), "adress"))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void nameNotNull() {
    assertThatThrownBy(() -> human.setName(null))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("name is null or empty");
  }

  @Test
  void testmaxDogsWithHumanFake() {
    Human human = new HumanFake();
    assertThatThrownBy(() -> human.addMultipleDogs(new DummyDog()))
        .isInstanceOf(TooMuchDogForOwner.class)
        .hasMessage("this human have more than 5 Dogs");
  }

  @Test
  void testWordGoodSynthax() {
    human.setName("robErt JEan-heUd DEZOKD");
    assertEquals(true, human.getName().matches(this.regexWord));
  }
}
