package co.simplon.alt3;

import java.time.LocalDate;

public class DummyDog extends Dog {

  public DummyDog() throws InvalidBirthdateException {
    super("name1", LocalDate.now(), 12);
  }

}
